cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "com.radaee.cordova.RadaeePDFPlugin",
      "file": "plugins/com.radaee.cordova/www/RadaeePDFPlugin.js",
      "pluginId": "com.radaee.cordova",
      "clobbers": [
        "RadaeePDFPlugin"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-whitelist": "1.3.4",
    "com.radaee.cordova": "1.6.5"
  };
});